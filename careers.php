<?php
/**
 * Template Name: Careers
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['careers'] = Timber::get_posts(array('post_type' => 'career', 'posts_per_page' => 5, 'orderby' => 'ASC'));

$templates = array( 'careers.twig' );

Timber::render( $templates, $context );