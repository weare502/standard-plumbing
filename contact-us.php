<?php
/**
 * Template Name: Contact
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['teamstaff'] = Timber::get_posts(array( 'post_type' => 'staff', 'posts_per_page' => 20, 'orderby' => 'title', 'order' => 'ASC' ));

$templates = array( 'contact.twig' );

Timber::render( $templates, $context );