<?php
/**
 * Template Name: Home
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['reviews'] = Timber::get_posts(array('post_type' => 'review', 'posts_per_page' => 3, 'orderby' => 'rand'));

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );