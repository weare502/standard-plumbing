<?php
// check for Timber
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

// change views dir to templates
Timber::$locations = __DIR__ . '/templates';

class StandardPlumbingSite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );

		add_action( 'init', array( $this, 'register_shortcake') );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'mce_custom_buttons') );

		add_action( 'init', function() {
			add_editor_style('style.css' );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['options'] = get_fields('option');
		$context['is_home'] = is_home();
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['plugin_content'] = TimberHelper::ob_function( 'the_content' );

		return $context;
	}

	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'footer', 'Footer Links Menu' );
		// Images Sizes
		add_image_size( 'xlarge', 2880, 2000 );
		add_image_size( 'large-square', 500, 500, true );

		acf_add_options_page(array(
			'page_title' 	=> 'Site Options',
			'menu_title'	=> 'Site Options',
			'menu_slug' 	=> 'spha-site-options',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}

	function enqueue_scripts() {
		// Dependencies
		wp_enqueue_style( 'spha-css', get_stylesheet_directory_uri() . "/style.css", array(), '20180503' );
		wp_enqueue_script( 'spha-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore' ), '20160823' );
		wp_enqueue_script( 'spha-vue', get_template_directory_uri() . "/static/js/vue.js", array( 'jquery', 'underscore' ), '20180302' );
		wp_enqueue_script( 'spha-service-request', get_template_directory_uri() . "/static/js/service-request-min.js", array( 'spha-vue', 'jquery-ui-datepicker' ), '20180102', true );
		// You need styling for the datepicker. For simplicity I've linked to Google's hosted jQuery UI CSS.
		wp_register_style( 'jquery-ui', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
		wp_enqueue_style( 'jquery-ui' );
	}

	function admin_head_css() {
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	// Custom Buttons added to WP Editor (MCE)
	// Button styles in "_buttons.scss" partial
	function mce_custom_buttons( $init_array ) {
		$style_formats = array(

			// Learn More Button
			array(
				'title' => 'Learn More Button',
				'block' => 'span',
				'classes' => 'learn-more',
				'wrapper' => false,
			),

			// White Background / Red Border button
			array(
				'title' => 'Red Border Button',
				'block' => 'span',
				'classes' => 'red-border-button',
				'wrapper' => false,
			),

			//  White Background / White Border / Red Text
			array(
				'title' => 'White Border Button',
				'block' => 'span',
				'classes' => 'white-fill-button',
				'wrapper' => false,
			),

			// Red Shadow Button
			array(
				'title' => 'Red Shadow Button',
				'block' => 'span',
				'classes' => 'red-shadow-button',
				'wrapper' => false,
			),
		);

		$init_array['style_formats'] = json_encode( $style_formats );
		return $init_array;
	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );
		$init_array['body_class'] .= " content ";
		return $init_array;  
	}

	// Custom Post Types go here
	function register_post_types() {
		// add cpts here
		require 'inc/post-type-reviews.php';
		require 'inc/post-type-staff.php';
		require 'inc/post-type-careers.php';
	}

	function register_shortcake() {
		// include 'inc/shortcake.php';
	}
}

// Add Excerpts for Cards
add_action( 'init', function() {
	add_post_type_support( 'page', 'excerpt' );
});

new StandardPlumbingSite();

// Main / Top Navigation Menu
function spha_render_primary_menu() { // used in base.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => false,
		// 'menu_class' => '',
		'menu_id' => 'primary-menu',
	) );
}

// Small Vertical Menu in Footer
function spha_render_footer_menu() {
	wp_nav_menu( array(
		'theme_location' => 'footer',
		'container' => false,
		// 'menu_class' => '',
		'menu_id' => 'footer-menu',
	) );
}