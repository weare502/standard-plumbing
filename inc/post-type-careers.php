<?php

$labels = array(
	'name'               => __( 'Job Descriptions', 'spha' ),
	'singular_name'      => __( 'Career', 'spha' ),
	'add_new'            => _x( 'Add New Opening', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Opening', 'spha' ),
	'edit_item'          => __( 'Edit Opening', 'spha' ),
	'new_item'           => __( 'New Opening', 'spha' ),
	'view_item'          => __( 'View Jobs', 'spha' ),
	'search_items'       => __( 'Search Openings', 'spha' ),
	'not_found'          => __( 'No Job penings found', 'spha' ),
	'not_found_in_trash' => __( 'No Job Openings found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Job Opening:', 'spha' ),
	'menu_name'          => __( 'Job Descriptions', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-groups',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'career', $args );