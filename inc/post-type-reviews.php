<?php

$labels = array(
	'name'               => __( 'Reviews', 'spha' ),
	'singular_name'      => __( 'Review', 'spha' ),
	'add_new'            => _x( 'Add New Review', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Review', 'spha' ),
	'edit_item'          => __( 'Edit Review', 'spha' ),
	'new_item'           => __( 'New Review', 'spha' ),
	'view_item'          => __( 'View Review', 'spha' ),
	'search_items'       => __( 'Search Reviews', 'spha' ),
	'not_found'          => __( 'No Reviews found', 'spha' ),
	'not_found_in_trash' => __( 'No Reviews found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Review:', 'spha' ),
	'menu_name'          => __( 'Reviews', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-star-half',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'review', $args );