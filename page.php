<?php
/**
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;
$templates = array( 'page.twig' );

$parent_id = $post->post_parent;
$children = array();

if ( $parent_id !== 0 ){
    $children = Timber::get_posts(
        array(
            'post_parent' => $parent_id, 
            'posts_per_page' => 1000, 
            'post_type' => 'page',
            'order' => 'ASC'
        )
    );
}

$context['children'] = $children;

the_post();

Timber::render( $templates, $context );