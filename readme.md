# Standard Plumbing Site

## Installation  
Before you can "install", you'll need a few tools.  

- `CodeKit 3`  
- `Timber for WordPress` plugin  
- `Gravity Forms` plugin  
- `Advanced Custom Fields Pro` plugin  
If you want to use Visual Studio Code, I have included my settings.json file and workspace setup inside the .vscode folder.  
You will need a few extensions (Ext Name - Dev Name)  
-> ESLint - Dirk Baeumer  
-> HTML Snippets - Mohamed Abusaid  
-> Path Intellisense - Christian Kohler  
-> SCSS Intellisense - mrmlnc  
-> Twig Language - mblode  
-> Vetur - Pine wu (if using vue.js)  
-> Liqube Dark Code - Liqube (My preffered theme - *similar* to Monokai). There is one that mimics Sublime's color theme (Monokai)  
-> Seti-Icons - qinjia (Icon tabs and Icons in file tree)  

If you use my theme; VSCode will throw an error if you try to use '!important' because having to use '!important' means your code is out of control.  

Codekit is used to compile CSS, minify JS, / Plugins are used for plugin things.  

### CSS  
- ALL variables go in the `_variables.scss` file. If you have a bazillion variables, ask me if you can split them up.  
- There should be one SCSS file for each custom page template or custom post type and they should be named appropriately. This makes finding CSS rules really really easy. CSS class names in this file should be named appropriately for the file and component they reference.  
- If you reuse a pattern in more than one place, then you can consider adding it to the `_global-patterns.scss` file. These class names should be fairly generic.  
- Media Queries should be placed inline on the element they affect if at all possible. *DO NOT place all the media queries at the bottom of the file.* Yo' momma raised you better than that!  

### JS  
- Most JS can go in the `site.js` file. jQuery and Underscore are included by default.  
- If you are developing a really in depth feature for a JS driven experience, create a new JS file and enqueue it normally. So basically if the interaction takes more than 10-20 lines of JS, you probably need a new file.  
- Vue.js apps should have their own file.  

### PHP  
- Custom Page Templates should generally have their own `.twig` files. It makes adding new code easy to reason about.  
- Single CPT templates should also have their own `.twig` files. So if you have a CPT for `Actors`, you would also have a `single-actor.twig` file.  
- When possible try to use the WordPress template heirachy before resorting to routing your templates via PHP.  