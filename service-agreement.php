<?php
/**
 * Template Name: Service Agreement
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$templates = array( 'service-agreement.twig' );

the_post();

Timber::render( $templates, $context );