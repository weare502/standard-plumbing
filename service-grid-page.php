<?php
/**
 * Template Name: Services Grid Page
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$children = Timber::get_posts( array( 
    'post_type' => $post->post_type, 
    'post_parent' => $post->ID, 
    'posts_per_page' => 1000,
    'order' => 'ASC'
) );

$context['children'] = $children;

$context['is_heating'] = is_page(229);
$context['is_cooling'] = is_page(245);

$templates = array( 'service-grid-page.twig' );

Timber::render( $templates, $context );