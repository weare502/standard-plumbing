"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* global Vue */
/* ignore ServiceRequestApp */
/* global Vue, jQuery */
/* ignore ServiceRequestApp */

/* eslint ignore:start */
/*
    jQuery Masked Input Plugin
    Copyright (c) 2007 - 2015 Josh Bush (digitalbush.com)
    Licensed under the MIT license (https://digitalbush.com/projects/masked-input-plugin/#license)
    Version: 1.4.1
*/
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? require("jquery") : jQuery);
}(function (a) {
    var b,
        c = navigator.userAgent,
        d = /iphone/i.test(c),
        e = /chrome/i.test(c),
        f = /android/i.test(c);a.mask = { definitions: { 9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]" }, autoclear: !0, dataName: "rawMaskFn", placeholder: "_" }, a.fn.extend({ caret: function caret(a, b) {
            var c;if (0 !== this.length && !this.is(":hidden")) return "number" == typeof a ? (b = "number" == typeof b ? b : a, this.each(function () {
                this.setSelectionRange ? this.setSelectionRange(a, b) : this.createTextRange && (c = this.createTextRange(), c.collapse(!0), c.moveEnd("character", b), c.moveStart("character", a), c.select());
            })) : (this[0].setSelectionRange ? (a = this[0].selectionStart, b = this[0].selectionEnd) : document.selection && document.selection.createRange && (c = document.selection.createRange(), a = 0 - c.duplicate().moveStart("character", -1e5), b = a + c.text.length), { begin: a, end: b });
        }, unmask: function unmask() {
            return this.trigger("unmask");
        }, mask: function mask(c, g) {
            var h, i, j, k, l, m, n, o;if (!c && this.length > 0) {
                h = a(this[0]);var p = h.data(a.mask.dataName);return p ? p() : void 0;
            }return g = a.extend({ autoclear: a.mask.autoclear, placeholder: a.mask.placeholder, completed: null }, g), i = a.mask.definitions, j = [], k = n = c.length, l = null, a.each(c.split(""), function (a, b) {
                "?" == b ? (n--, k = a) : i[b] ? (j.push(new RegExp(i[b])), null === l && (l = j.length - 1), k > a && (m = j.length - 1)) : j.push(null);
            }), this.trigger("unmask").each(function () {
                function h() {
                    if (g.completed) {
                        for (var a = l; m >= a; a++) {
                            if (j[a] && C[a] === p(a)) return;
                        }g.completed.call(B);
                    }
                }function p(a) {
                    return g.placeholder.charAt(a < g.placeholder.length ? a : 0);
                }function q(a) {
                    for (; ++a < n && !j[a];) {}return a;
                }function r(a) {
                    for (; --a >= 0 && !j[a];) {}return a;
                }function s(a, b) {
                    var c, d;if (!(0 > a)) {
                        for (c = a, d = q(b); n > c; c++) {
                            if (j[c]) {
                                if (!(n > d && j[c].test(C[d]))) break;C[c] = C[d], C[d] = p(d), d = q(d);
                            }
                        }z(), B.caret(Math.max(l, a));
                    }
                }function t(a) {
                    var b, c, d, e;for (b = a, c = p(a); n > b; b++) {
                        if (j[b]) {
                            if (d = q(b), e = C[b], C[b] = c, !(n > d && j[d].test(e))) break;c = e;
                        }
                    }
                }function u() {
                    var a = B.val(),
                        b = B.caret();if (o && o.length && o.length > a.length) {
                        for (A(!0); b.begin > 0 && !j[b.begin - 1];) {
                            b.begin--;
                        }if (0 === b.begin) for (; b.begin < l && !j[b.begin];) {
                            b.begin++;
                        }B.caret(b.begin, b.begin);
                    } else {
                        for (A(!0); b.begin < n && !j[b.begin];) {
                            b.begin++;
                        }B.caret(b.begin, b.begin);
                    }h();
                }function v() {
                    A(), B.val() != E && B.change();
                }function w(a) {
                    if (!B.prop("readonly")) {
                        var b,
                            c,
                            e,
                            f = a.which || a.keyCode;o = B.val(), 8 === f || 46 === f || d && 127 === f ? (b = B.caret(), c = b.begin, e = b.end, e - c === 0 && (c = 46 !== f ? r(c) : e = q(c - 1), e = 46 === f ? q(e) : e), y(c, e), s(c, e - 1), a.preventDefault()) : 13 === f ? v.call(this, a) : 27 === f && (B.val(E), B.caret(0, A()), a.preventDefault());
                    }
                }function x(b) {
                    if (!B.prop("readonly")) {
                        var c,
                            d,
                            e,
                            g = b.which || b.keyCode,
                            i = B.caret();if (!(b.ctrlKey || b.altKey || b.metaKey || 32 > g) && g && 13 !== g) {
                            if (i.end - i.begin !== 0 && (y(i.begin, i.end), s(i.begin, i.end - 1)), c = q(i.begin - 1), n > c && (d = String.fromCharCode(g), j[c].test(d))) {
                                if (t(c), C[c] = d, z(), e = q(c), f) {
                                    var k = function k() {
                                        a.proxy(a.fn.caret, B, e)();
                                    };setTimeout(k, 0);
                                } else B.caret(e);i.begin <= m && h();
                            }b.preventDefault();
                        }
                    }
                }function y(a, b) {
                    var c;for (c = a; b > c && n > c; c++) {
                        j[c] && (C[c] = p(c));
                    }
                }function z() {
                    B.val(C.join(""));
                }function A(a) {
                    var b,
                        c,
                        d,
                        e = B.val(),
                        f = -1;for (b = 0, d = 0; n > b; b++) {
                        if (j[b]) {
                            for (C[b] = p(b); d++ < e.length;) {
                                if (c = e.charAt(d - 1), j[b].test(c)) {
                                    C[b] = c, f = b;break;
                                }
                            }if (d > e.length) {
                                y(b + 1, n);break;
                            }
                        } else C[b] === e.charAt(d) && d++, k > b && (f = b);
                    }return a ? z() : k > f + 1 ? g.autoclear || C.join("") === D ? (B.val() && B.val(""), y(0, n)) : z() : (z(), B.val(B.val().substring(0, f + 1))), k ? b : l;
                }var B = a(this),
                    C = a.map(c.split(""), function (a, b) {
                    return "?" != a ? i[a] ? p(b) : a : void 0;
                }),
                    D = C.join(""),
                    E = B.val();B.data(a.mask.dataName, function () {
                    return a.map(C, function (a, b) {
                        return j[b] && a != p(b) ? a : null;
                    }).join("");
                }), B.one("unmask", function () {
                    B.off(".mask").removeData(a.mask.dataName);
                }).on("focus.mask", function () {
                    if (!B.prop("readonly")) {
                        clearTimeout(b);var a;E = B.val(), a = A(), b = setTimeout(function () {
                            B.get(0) === document.activeElement && (z(), a == c.replace("?", "").length ? B.caret(0, a) : B.caret(a));
                        }, 10);
                    }
                }).on("blur.mask", v).on("keydown.mask", w).on("keypress.mask", x).on("input.mask paste.mask", function () {
                    B.prop("readonly") || setTimeout(function () {
                        var a = A(!0);B.caret(a), h();
                    }, 0);
                }), e && f && B.off("input.mask").on("input.mask", u), A();
            });
        } });
});
/* eslint ignore:end */

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

Vue.directive('mask', function (el, binding) {
    jQuery(el).mask(binding.value.mask, {
        completed: function completed() {
            ServiceRequestApp[binding.value.model] = jQuery(this).val();
        }
    });
});

var datePickerStartDate = function datePickerStartDate() {
    var d = new Date();
    var r = "1";
    switch (d.getDay()) {
        case 0:
            r = "2";
            break;
        case 5:
            r = "4";
            break;
        case 6:
            r = "3";
            break;
        default:
            break;
    }
    return r;
};

if (document.getElementById('service-request')) {
    var ServiceRequestApp = new Vue({ // eslint-disable-line no-unused-vars
        el: '#service-request',
        data: {
            services: [{
                name: 'Plumbing',
                selected: false
            }, {
                name: 'Drain & Sewer',
                selected: false
            }, {
                name: 'Air Conditioning',
                selected: false
            }, {
                name: 'Heating',
                selected: false
            }, {
                name: 'Free Assessment',
                selected: false
            }],
            date: '',
            time: '',
            name: {
                first: '',
                last: ''
            },
            email: '',
            emailConfirm: '',
            phoneNumber: '',
            contactMethod: '',
            serviceAddress: {
                street: '',
                street2: '',
                city: '',
                state: '',
                zip: ''
            },
            comments: '',
            submitted: false,
            errors: [],
            submitting: false,
            dateTimeOpen: false,
            contactInfoOpen: false
        },
        methods: {
            expandSection: function expandSection(event) {
                var button = jQuery(event.target);

                button.siblings('.form-section-inner').slideDown();
                button.addClass('opened').attr('disabled', true);
            },
            expandDateTime: function expandDateTime() {
                if (this.dateTimeOpen) {
                    return;
                }

                var button = jQuery('#date-time .form-section-header');

                button.siblings('.form-section-inner').slideDown();
                button.addClass('opened').attr('disabled', true);

                this.dateTimeOpen = true;
            },
            expandContactInfo: function expandContactInfo() {
                if (this.contactInfoOpen) {
                    return;
                }

                var button = jQuery('#contact-info .form-section-header');

                button.siblings('.form-section-inner').slideDown();
                button.addClass('opened').attr('disabled', true);

                this.contactInfoOpen = true;
            },
            checkDateTime: function checkDateTime() {
                if (this.date && this.time) {
                    this.expandContactInfo();
                }
            },
            submitForm: function submitForm() {
                if (this.submitting) return;
                // clear all errors before we compile again
                this.errors = [];

                if (!this.selectedServices.length) this.errors.push('Please select a service.');

                if (!this.date) this.errors.push('Please enter a service date.');

                if (!this.time) this.errors.push('Please select a service time.');

                if (!this.name.first || !this.name.last) this.errors.push('Please provide your full name.');

                if (!this.email) this.errors.push('Please provide an email address.');

                if (this.email !== this.emailConfirm) this.errors.push('Email addresses do not match.');

                if (!this.phoneNumber) this.errors.push('Please provide a phone number.');

                if (!this.serviceAddress.street) this.errors.push('Please enter a street address.');

                if (!this.serviceAddress.city) this.errors.push('Please enter a city.');

                if (!this.errors.length) {
                    this.submitting = true;
                    this.ajaxSubmitForm();
                }
            },
            CalculateSig: function CalculateSig(stringToSign, privateKey) {
                //calculate the signature needed for authentication
                var hash = CryptoJS.HmacSHA1(stringToSign, privateKey);
                var base64 = hash.toString(CryptoJS.enc.Base64);
                return encodeURIComponent(base64);
            },
            ajaxSubmitForm: function ajaxSubmitForm() {
                //set variables
                var d = new Date();
                var expiration = 3600; // 1 hour,
                var unixtime = parseInt(d.getTime() / 1000);
                var future_unixtime = unixtime + expiration;
                var publicKey = "cc2f2d034f";
                var privateKey = "31c7172c6787cb9";
                var method = "POST";
                var route = "forms/1/submissions";

                var stringToSign = publicKey + ":" + method + ":" + route + ":" + future_unixtime;
                var sig = this.CalculateSig(stringToSign, privateKey);
                var url = '/gravityformsapi/' + route + '?api_key=' + publicKey + '&signature=' + sig + '&expires=' + future_unixtime;

                var values = {
                    input_values: {
                        "input_1": this.name.first + ' ' + this.name.last,
                        "input_2_1": this.serviceAddress.street,
                        "input_2_2": this.serviceAddress.street2,
                        "input_2_3": this.serviceAddress.city,
                        "input_2_5": this.serviceAddress.zip,
                        "input_3": this.phoneNumber,
                        "input_4": this.email,
                        "input_5": this.date,
                        "input_6": this.time,
                        "input_7": this.servicesList,
                        "input_8": this.comments
                    }
                };
                //json encode array
                var values_json = JSON.stringify(values);
                var self = this;
                jQuery.post(url, values_json, function (data) {
                    if (data.response.is_valid) {
                        self.submitting = false;
                        self.submitted = true;
                    } else {
                        self.errors.push('Error submitting request. Please call for service or try again later.');
                        self.submitting = false;
                    }
                });
            }
        },
        computed: {
            selectedServices: function selectedServices() {
                return this.services.filter(function (service) {
                    return service.selected;
                });
            },
            servicesList: function servicesList() {
                return this.selectedServices.map(function (x) {
                    return x.name;
                }).join(', ');
            },
            isSubmitting: function isSubmitting() {
                return this.submitting ? true : false;
            }
        },
        watch: {
            services: {
                handler: function handler() {
                    if (this.selectedServices.length) {
                        this.expandDateTime();
                    }
                },
                deep: true
            },
            date: 'checkDateTime',
            time: 'checkDateTime'
        },
        mounted: function mounted() {
            var self = this;
            var city = getUrlParameter('location');
            var service = getUrlParameter('service-type');

            if (city) {
                this.serviceAddress.city = city;
                this.serviceAddress.state = "Kansas";
            }

            if (service) {
                this.services.forEach(function (el, i) {
                    if (el.name === service) {
                        el.selected = true;
                    }
                });
            }
            jQuery('#service-request-date').datepicker({
                beforeShowDay: jQuery.datepicker.noWeekends,
                dateFormat: 'M d, yy',
                minDate: datePickerStartDate(),
                maxDate: "+14d",
                onClose: function onClose() {
                    jQuery(this).blur();
                    self.date = this.value;
                }
            });
        }
    });
}
