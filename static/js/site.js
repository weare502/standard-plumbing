/* global jQuery */
(function($){
    $(document).ready(kickoff);

    function kickoff(){
        $('#menu-toggle').click(function(){
            $('#primary-menu').toggleClass('menu-open');
        });
    }
})(jQuery)